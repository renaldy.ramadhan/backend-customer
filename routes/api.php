<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("v1/")->group(function () {
    Route::get("customers", [App\Http\Controllers\API\CustomerController::class, "index"]);
    Route::post("customer", [App\Http\Controllers\API\CustomerController::class, "store"]);
    Route::get("customer/{id}", [App\Http\Controllers\API\CustomerController::class, "show"]);
    Route::put("customer/{id}", [App\Http\Controllers\API\CustomerController::class, "update"]);
    Route::delete("customer/delete/{id}", [App\Http\Controllers\API\CustomerController::class, "destroy"]);
});
