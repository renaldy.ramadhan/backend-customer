<?php

namespace App\Service;

use Illuminate\Http\JsonResponse;

class ResponseAPI
{
    private static $response = [
        "statusCode" => 200,
        "message" => "",
        "data" => null
    ];

    public static function generate($status, $message, $data = null): JsonResponse
    {
        self::$response['statusCode'] = $status;
        self::$response['message'] = $message;
        self::$response['data'] = $data;

        return response()->json(self::$response, $status);
    }
}
