<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Service\ResponseAPI;
use Illuminate\Http\Request;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $customers = Customer::all();
        return ResponseAPI::generate(200, "Customer fetched", $customers);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $customer = Customer::create($request->all());
        return ResponseAPI::generate(201, "Resource created", $customer);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $customer = Customer::findOrFail($id);
            return ResponseAPI::generate(200, "Detail fetched", $customer);
        } catch (ModelNotFoundException $e) {
            return ResponseAPI::generate(404, $e->getMessage());
        } catch (\Exception $e) {
            return ResponseAPI::generate(500, "Error response from backend customer!");
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $customer = Customer::findOrFail($id);
            $customer->update($request->all());
            return ResponseAPI::generate(200, "Row Updated!", $customer);
        } catch (ModelNotFoundException $e) {
            return ResponseAPI::generate(404, $e->getMessage());
        } catch (\Exception $e) {
            return ResponseAPI::generate(500, "Error response from backend customer!");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $customer = Customer::findOrFail($id);
            $customer->delete();
            return ResponseAPI::generate(200, "Row Deleted!");
        } catch (ModelNotFoundException $e) {
            return ResponseAPI::generate(404, $e->getMessage());
        } catch (\Exception $e) {
            return ResponseAPI::generate(500, "Error response from backend customer!");
        }
    }
}
